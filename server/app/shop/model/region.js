module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_region",
    hasTimestamps: true,
    softDelete: true
});
