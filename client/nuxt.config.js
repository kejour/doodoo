// const apiHost = "http://api.doodooke.com";
const apiHost = "http://127.0.0.1:3001";

module.exports = {
    /*
    ** Headers of the page
    */
    head: {
        title:
            "多多客小程序_微信小程序_微信小程序开发_微信小程序工具_微信小程序制作平台",
        meta: [
            { charset: "utf-8" },
            {
                name: "viewport",
                content: "width=device-width, initial-scale=1"
            },
            {
                hid: "description",
                name: "description",
                content:
                    "多多客doodooke是国内领先的微信小程序开发平台，制作过程无需代码，可视化拖拽组件即可，提供海量小程序行业模板，联合服务商、开发者、运营专家，共建服务生态，服务百万商家。"
            },
            {
                hid: "keywords",
                name: "keywords",
                content:
                    "多多客，多多客小程序，微信小程序，小程序，微信小程序开发，微信小程序平台，小程序开发，小程序制作，小程序api，小程序开发工具，小程序开发平台，微信小程序开发工具，微信小程序制作"
            }
        ],
        script: [
            {
                src:
                    "https://cdn.bootcss.com/babel-polyfill/6.23.0/polyfill.min.js"
            },
            { src: "https://cdn.bootcss.com/socket.io/2.0.4/socket.io.js" },
            {
                src:
                    "https://hm.baidu.com/hm.js?be01837c791802c529c8c6890dfe5079"
            }
        ],
        link: [
            { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
            {
                rel: "stylesheet",
                type: "text/css",
                href: "https://cdn.bootcss.com/animate.css/3.5.2/animate.css"
            }
        ]
    },
    /*
    ** Customize the progress bar color
    */
    loading: { color: "#3B8070" },
    /*
    ** Build configuration
    */
    build: {
        /*
        ** Run ESLint on save
        */
        extend(config, { isDev, isClient }) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: "pre",
                    test: /\.(js|vue)$/,
                    loader: "eslint-loader",
                    exclude: /(node_modules)/
                });
            }
            config.devtool = "#source-map";
        },

        vendor: ["axios", "element-ui"]
    },

    loading: false,

    modules: ["@nuxtjs/axios", "cookie-universal-nuxt", "@nuxtjs/sitemap"],

    axios: {
        proxy: true, // Can be also an object with default options
        debug: false,
        baseURL: `http://0.0.0.0:3000`,
        proxyHeaders: {}
    },

    router: {
        middleware: ["router", "auth"]
    },

    sitemap: {
        path: "/sitemap.xml",
        hostname: "http://www.doodooke.com",
        cacheTime: 1000 * 60 * 15,
        gzip: true,
        generate: false, // Enable me when using nuxt generate
        exclude: ["**"],
        routes: [
            "/",
            "/portal",
            "/portal/diy",
            "/portal/shop",
            "/public/login",
            "/public/register",
            "/public/forget_pwd",
            "/cms/cms",
            "/www/diy.html",
            "/www/shop.html"
        ]
    },

    proxy: {
        "/api": { target: apiHost, pathRewrite: { "^/api": "/" } }
    },

    css: [
        "element-ui/lib/theme-chalk/index.css",
        "./static/common.css",
        "./static/font/iconfont.css"
    ],

    plugins: [
        { src: "~plugins/element.js", ssr: true },
        { src: "~plugins/axios.js", ssr: true },
        { src: "~plugins/util.js", ssr: true },
        { src: "~plugins/upload.js", ssr: false },
        { src: "~plugins/link.js", ssr: false },
        { src: "~plugins/editor.js", ssr: false },
        { src: "~plugins/amap.js", ssr: false },
        { src: "~plugins/particles.js", ssr: false },
        { src: "~plugins/echarts.js", ssr: false },
        { src: "~plugins/region.js", ssr: false },
        { src: "~plugins/error.js", ssr: false },
        { src: "~plugins/ga.js", ssr: false },
        { src: "~plugins/dragx.js", ssr: false },
        { src: "~plugins/bus.js", ssr: false }
    ]
};
